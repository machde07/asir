


----ORACLE -----

create table compositores
(
ID varchar2(5),
Nombre varchar2(50),
FechaNacimiento date,
Nacionalidad varchar2(30),
URLBiografia varchar2(100),
constraint fk_compositores primary key(ID),
constraint nacimiento check(to_char(FechaNacimiento, 'YYYY-MM-DD') between '1290-01-01' and
'1920-12-31'),
constraint url check(regexp_like(URLBiografia,'(www\.)([A-Za-z0-9  ]*)\.(com|es|org|net)$')));


insert into compositores values ('01','LUDWING VAN BEETHOWEN','16-12-1770','alemana','www.ludwingvanbeethowen.org');
insert into compositores values ('02','WOLFGANG AMADEUS MOZART','27-01-1756','austriaca','www.mozart.com');
insert into compositores values ('03','ANTONIO VIVALDI','04-03-1678','italiana','www.antoniovivaldi.net');
insert into compositores values ('04','FRÉDERIC CHOPIN','01-03-1810','polaca','www.fredericchopin.com');
insert into compositores values ('05','MANUEL DE FALLA','23-11-1876','espanola','www.manueldefalla.com');
insert into compositores values ('06','FRANZ SCHUBERT','31-01-1797','austriaca','www.franzschubert.com');
insert into compositores values ('07','LOUISS SPOHR','05-04-1784','alemana','www.louisspohr.com');
insert into compositores values ('08','ROBERT SCHUMANN','08-06-1810','alemana','www.robertschumann.es');






---OBRAS----
create table obras
(
codigo_obra varchar2(6),
URL varchar2(100),
nombre_obra varchar2(50),
duracion varchar2(12),
facil_escucha varchar2(2),
constraint fk_obras primary key(codigo_obra),
constraint clase_codigo check(regexp_like(codigo_obra, '([ABCMR][0-9]{1,4}|[ABCMR][0-9]{1,4}[A-Za-z]$)')),
constraint si_o_no check(regexp_like(facil_escucha'si|no')),
constraint dura_obra check(regexp_like(duracion,  '^[0-9]{1,3}\`[0-9]{2}\`\`')));

insert into obras values ('R1234B','https://www.youtube.com/watch?v=LP5_0mWM_ko','FUR ELISE','05`07``','no');
insert into obras values ('R1235B','https://www.youtube.com/watch?v=gkDbAWKkeX4&t=467s','QUINTA SINFONIA','34`50``','no');
insert into obras values ('C1236M','https://www.youtube.com/watch?v=qch9mpigHUc','SERENATA Nº13','11`55``','no');
insert into obras values ('R1237V','https://www.youtube.com/watch?v=eY1Xivm1CXE&t=1265s','CUATRO ESTACIONES','51`23``','si');
insert into obras values ('R1238C','https://www.youtube.com/watch?v=sg3DsKtZVHM','MARCHA FUNEBRE','09`19``','no');
insert into obras values ('R1239F','https://www.youtube.com/watch?v=XMALVK_I8hE&t=405s','EL AMOR BRUJO','23`55``','no');
insert into obras values ('R1240S','https://www.youtube.com/watch?v=4PBZ2KcaU0U','LA TRUCHA','12`43``','no');
insert into obras values ('R1245S','https://www.youtube.com/watch?v=ApO6JTEFOqo&t=12s','CARNAVAL','10`48``','si');
insert into obras values ('R1242P','https://www.youtube.com/watch?v=7g8_3avJs3o&t=21s','SYMPHONY 6','27`05``','no');
insert into obras values ('R1243B','https://www.youtube.com/watch?v=mtHKQWY6m80','CLARO DE LUNA','17`27``','no');
insert into obras values ('C1276M','https://www.youtube.com/watch?v=t3217H8JppI&t=1817s','Symphony No. 9','10`08``','no');
insert into obras values ('C12M','https://www.youtube.com/watch?v=YFD2PPAqNbw','SILENCE','09`55``','no');



---compositoresporpiezas---
create table compositoresporpiezas
(
idcompositor varchar2(5),
cod_obra varchar2(6),
constraint fk_compositorporpiezas primary key (idcompositor,cod_obra),
constraint pk_compositores foreign key (idcompositor) references compositores(ID),
constraint pk_obras foreign key (cod_obra) references obras(codigo_obra));

insert into compositoresporpiezas values('01','R1234B');
insert into compositoresporpiezas values('01','R1235B');
insert into compositoresporpiezas values('02','C1236M');
insert into compositoresporpiezas values('03','R1237V');
insert into compositoresporpiezas values('04','R1238C');
insert into compositoresporpiezas values('05','R1239F');
insert into compositoresporpiezas values('06','R1240S');
insert into compositoresporpiezas values('01','R1242P');
insert into compositoresporpiezas values('07','R1243B');
insert into compositoresporpiezas values('01','C12M');
insert into compositoresporpiezas values('01','C1276M');
insert into compositoresporpiezas values('08','R1245S');


---temporadas---
create table temporadas
(
nombre_temporada varchar2(25),
fecha_inicio date,
fecha_fin date,
constraint fk_temporada primary key (nombre_temporada));

insert into temporadas values ('verano','21/06/2018','21/09/2018');
insert into temporadas values ('primavera','21/03/2018','21/06/2018');
insert into temporadas values ('otoño/invierno','21/09/2017','21/03/2018');









----ciclos----
create table ciclos
(
cod_ciclo varchar2(3),
titulo varchar2(50),
homenaje varchar2(50),
constraint fk_ciclos primary key(cod_ciclo));

insert into ciclos values ('a12','GRANDES AUTORES','autores s.XVIII');
insert into ciclos values ('b13','SINFONIAS','Bethoween');
insert into ciclos values ('b14','EMSEMBLE MATHEUS','Antonio Vivaldi');

---conciertos---
create table conciertos
(
cod_concierto varchar2(6),
temporada varchar2(25),
codigo_ciclo varchar2(3),
titulo_concierto varchar2(50),
fechahora date,
constraint fk_concierto primary key(cod_concierto),
constraint pk_temporada foreign key (temporada) references temporadas(nombre_temporada),
constraint pk_codigo_ciclo foreign key (codigo_ciclo) references ciclos(cod_ciclo),
constraint tipo_codigo_concierto check(regexp_like(cod_concierto, '[a-z]([0-9]{3}|[0-9]{3}\-[a-z])$')),
CONSTRAINT CK_fecha CHECK  ( ( TO_CHAR(fechahora,'D') IN ('2','3','4','5','6') ) AND ( TO_CHAR(fechahora,'HH24') > '20' ) OR (  TO_CHAR(fechahora,'D') IN ('1','7') ) AND  (TO_CHAR(fechahora,'MM-DD') BETWEEN '06-21' AND '09-21' ) AND (TO_CHAR(fechahora,'HH24') > '10')  OR  (TO_CHAR(fechahora,'MM-DD') NOT BETWEEN '06-21' AND '09-21') AND (TO_CHAR(fechahora,'HH24') > '12') ) ) ;


insert into conciertos values ('a345-a','primavera','b12','PHILHARMONIE I:GRANDE salle Pierre Boulez',to_date('2018-04-12 21:00 ','YYYY-MM-DD HH24:MI'));
insert into conciertos values ('b984','verano','b11','National Concert Hall',to_date('2018-07-12 22:00 ','YYYY-MM-DD HH24:MI'));
insert into conciertos values ('z125','otoño/invierno','b13','La Scala',to_date('2017-12-18 20:30 ','YYYY-MM-DD HH24:MI'));
insert into conciertos values ('v567-t','verano','b14','Bolero Ravel',to_date(' 2018-09-22 17:00 ','YYYY-MM-DD HH24:MI'));
insert into conciertos values ('v657-l','primavera','b15','IN DUE CORI RV 585',to_date('2018-03-28 20:00 ','YYYY-MM-DD HH24:MI'));
insert into conciertos values ('v456-p','otoño/invierno','b16','PHILHARMONIE I:GRANDE salle Pierre Boulez',to_date('2018-10-12 22:00 ','YYYY-MM-DD HH24:MI'));
insert into conciertos values ('g567-j','verano','a16','sinfonia nº7',to_date('2018-07-12 23:00 ','YYYY-MM-DD HH24:MI'));
insert into conciertos values ('g351-f','primavera','a17','sinfonia nº5',to_date('2018-12-04 21:00 ','YYYY-MM-DD HH24:MI'));
insert into conciertos values ('g198-g','otoño/invierno','a18','EINE KLEINE NATCHTMUSIK',to_date('2018-11-25 21:30 ','YYYY-MM-DD HH24:MI'));



---programas---

create table programas
(
codig_concierto varchar2(6),
obras_codigo varchar2(6),
orden integer,
constraint pk_programas primary key(codig_concierto, obras_codigo),
constraint p_codconciertos foreign key (codig_concierto) references conciertos(cod_concierto),
constraint p_obras_codigo foreign key (obras_codigo) references obras (codigo_obra),
constraint max_obras check(orden between 1 and 12));

insert into programas values ('a345-a','R1235B','1');
insert into programas values ('a345-a','R1234B','2');
insert into programas values ('a345-a','R1243B','3');
insert into programas values ('a345-a','C1236M','4');
insert into programas values ('a345-a','R1237V','5');
insert into programas values ('a345-a','R1242P','6');
insert into programas values ('a345-a','C1276M','7');
insert into programas values ('a345-a','C12M','8');
insert into programas values ('a345-a','R1240S','9');





insert into programas values ('b984','R1235B','1');
insert into programas values ('b984','R1245S','2');
insert into programas values ('b984','R1239F','3');
insert into programas values ('b984','R1243B','4');
insert into programas values ('b984','C1276M','5');
insert into programas values ('b984','C12M','6';
insert into programas values ('b984','R1238C','7');
insert into programas values ('b984','C1236M','8');
insert into programas values ('b984','R1237V','9');
insert into programas values ('b984','R1240S','10');
insert into programas values ('b984','R1242P','11');

insert into programas values ('v456-p','R1235B','1');
insert into programas values ('v456-p','R1234B','2');
insert into programas values ('v456-p','R1243B','3');
insert into programas values ('v456-p','C1236M','4');
insert into programas values ('v456-p','R1237V','5');
insert into programas values ('v456-p','R1242P','6');
insert into programas values ('v456-p','C1276M','7');
insert into programas values ('v456-p','C12M','8');
insert into programas values ('v456-p','R1240S','9');



---interpretes---


create table interpretes
(
id_interprete varchar2(10),
nombre_interprete varchar2(50),
constraint pk_interpretes primary key (id_interprete),
constraint contar check(length(id_interprete)>='5'));

insert into interpretes values ('123456','MAURICE ANDRE');
insert into interpretes values ('1er456','CLAUDIO ARRAU');
insert into interpretes values ('654tgb','DANIEL BARENBOIM');
insert into interpretes values ('543frtyh','YURI BASHMET');
insert into interpretes values ('12654f','HEINZ HOLLIGER');
insert into interpretes values ('qazwsxedc','VLADIMIR HOROWITZ');
insert into interpretes values ('1mkonji','YEHUDI MENUHIN');
insert into interpretes values ('1234567','MAURICE ANDRE');
insert into interpretes values ('123345','DAVID OISTRAKH');
insert into interpretes values ('123321','ITZHAK PERLMAN');
insert into interpretes values ('123565','MSTISLAV ROSTROPOVICH');
insert into interpretes values ('123422','Marc-André Hamelin ');
insert into interpretes values ('1234777','PIERRE RAMPAL');
insert into interpretes values ('124223','RUBEN FERNANDEZ');
insert into interpretes values ('12bgtyhn','Martha Argerich');




---interpretesporpiezas---

create table interpretesporpiezas
(
interprete_id varchar2(10),
COD_obra varchar2(6),
COD_concierto varchar2(6),
constraint pk_interpretesporpiezas primary key( interprete_id,COD_obra, COD_concierto),
constraint intercod_interprete foreign key (interprete_id) references interpretes(id_interprete),
constraint intercod_con foreign key (COD_concierto,COD_obra) references programas (codig_concierto,obras_codigo)
);


insert into interpretesporpiezas values ('123456','R1235B','b984');
insert into interpretesporpiezas values ('123456','R1234B','a345-a');
insert into interpretesporpiezas values ('1mkonji','R1243B','a345-a');
insert into interpretesporpiezas values ('1234567','C1236M','a345-a');
insert into interpretesporpiezas values ('654tgb','R1237V','a345-a');
insert into interpretesporpiezas values ('654tgb','R1242P','a345-a');
insert into interpretesporpiezas values ('654tgb','C12M','a345-a');
insert into interpretesporpiezas values ('qazwsxedc','R1235B','a345-a');
insert into interpretesporpiezas values ('qazwsxedc','R1235B','b984');
insert into interpretesporpiezas values ('qazwsxedc','R1238C','a345-a');
insert into interpretesporpiezas values ('123345','R1235B','a345-a');
insert into interpretesporpiezas values ('123345','R1238C','b984');
insert into interpretesporpiezas values ('123345','C1236M','a345-a');
insert into interpretesporpiezas values ('123565','C1236M','b984');
insert into interpretesporpiezas values ('1234567','C1236M','b984');
insert into interpretesporpiezas values ('123565','R1240S','v456-p');
insert into interpretesporpiezas values ('543frtyh','R1240S','v456-p');
insert into interpretesporpiezas values ('1mkonji','R1240S','b984');
insert into interpretesporpiezas values ('1mkonji','R1242P','b984');
insert into interpretesporpiezas values ('123422','R1242P','b984');
insert into interpretesporpiezas values ('12bgtyhn','R1242P','v456-p');
insert into interpretesporpiezas values ('543frtyh','R1242P','v456-p');
insert into interpretesporpiezas values ('123422','R1239F','b984');
insert into interpretesporpiezas values ('543frtyh','R1239F','b984');
insert into interpretesporpiezas values ('12bgtyhn','R1239F','b984');
insert into interpretesporpiezas values ('12bgtyhn','R1245S','b984');
insert into interpretesporpiezas values ('123422','R1235B','b984');









---solistas---
create table solistas
(
IDinterprete_solista varchar2(10),
nombre_artistico varchar2(50),
instrumento varchar2(100),
constraint pk_solistas primary key (IDinterprete_solista),
constraint solistas_interpretes foreign key (IDinterprete_solista) references interpretes(id_interprete));



insert into solistas values ('124223','RUBEN FERNANDEZ','flauta');
insert into solistas values ('123456','MAURICE ANDRE','trompeta');
insert into solistas values ('1er456','CLAUDIO ARRAU','piano');
insert into solistas values ('654tgb','BARENBOIM','piano');
insert into solistas values ('543frtyh','YURI BASHMET','violin');
insert into solistas values ('12654f','HEINZ HOLLIGER','oboe');
insert into solistas values ('qazwsxedc','HOROWITZ','piano');
insert into solistas values ('1mkonji','MENUHIN','violin');
insert into solistas values ('1234567','PIERRE RAMPAL','flauta');
insert into solistas values ('123345','OISTRAKH','violin');


---ORQUESTAS---
create table orquestas
(
IDinterprete_orquesta varchar2(10),
num_musicos integer,
director varchar2(100),
constraint pk_solistas_orq primary key (IDinterprete_orquesta),
constraint solistas_interpretes_orq foreign key (IDinterprete_orquesta) references interpretes(id_interprete),
constraint num_solistas check(num_musicos between 10 and 90));


insert into orquestas values ('123321','89','Daniele Gatti ');
insert into orquestas values ('123565','77','Yuri Simonov');
insert into orquestas values ('123422','55','RICCARDO MUTI');
insert into orquestas values ('124223','32','RUBEN FERNANDEZ');
insert into orquestas values ('12bgtyhn','81','Mtro. Felipe Izcaray');

---repertoriosolistas---

create table repertoriosolistas
(
id_interprete_solista varchar2(10),
CODobra_solista varchar2(10),
constraint pk_reper_solistas primary key(id_interprete_solista, CODobra_solista),
constraint fk__codigo_interprete_solista foreign key (id_interprete_solista) references solistas(IDinterprete_solista),
constraint fk_codigo_obra_solista foreign key (CODobra_solista) references obras(codigo_obra));

insert into repertoriosolistas values ('123456','R1234B');
insert into repertoriosolistas values ('123456','R1235B');
insert into repertoriosolistas values ('654tgb','R1237V');
insert into repertoriosolistas values ('543frtyh','R1242P');
insert into repertoriosolistas values ('qazwsxedc','R1238C');
insert into repertoriosolistas values ('543frtyh','R1239F');
insert into repertoriosolistas values ('1mkonji','R1240S');
insert into repertoriosolistas values ('1234567','C1276M');
insert into repertoriosolistas values ('654tgb','C12M');



insert into repertoriosolistas values ('1er456','R1235B');
insert into repertoriosolistas values ('654tgb','R1238C');
insert into repertoriosolistas values ('543frtyh','C12M');
insert into repertoriosolistas values ('12654f','R1238C');
insert into repertoriosolistas values ('qazwsxedc','R1240S');
insert into repertoriosolistas values ('1mkonji','C12M');
insert into repertoriosolistas values ('1mkonji','R1239F');
insert into repertoriosolistas values ('654tgb','R1242P');
insert into repertoriosolistas values ('543frtyh','R1238C');
insert into repertoriosolistas values ('543frtyh','C1276M');
