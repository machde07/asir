############################################################################

VPN de acceso remoto con OpenVPN y certificados x509

############################################################################
############################################################################

Empezaremos creando el siguiente Vagrantfile

<pre>
Vagrant.configure("2") do |config|
 
  config.vm.define :clientevpn do |clientevpn|
    clientevpn.vm.box = "debian/buster64"
    clientevpn.vm.hostname = "clientevpn"
    clientevpn.vm.network :public_network, :bridge=>"wlp3s0f0"

  end

  config.vm.define :servidorvpn do |servidorvpn|
    servidorvpn.vm.box = "debian/buster64"
    servidorvpn.vm.hostname = "servidorvpn"
    servidorvpn.vm.network :public_network, :bridge=>"wlp3s0f0"
    servidorvpn.vm.network :private_network, ip:"192.168.105.2", virtualbox__intnet: "Red_interna"

  end

  config.vm.define :clienteint do |clienteint|
    clienteint.vm.box = "debian/buster64"
    clienteint.vm.hostname = "cliente-int1"
    clienteint.vm.network :private_network, ip:"192.168.105.10", virtualbox__intnet: "Red_interna"

  end

  config.vm.define :clienteint1 do |clienteint1|
    clienteint1.vm.box = "debian/buster64"
    clienteint1.vm.hostname = "cliente-int2"
    clienteint1.vm.network :private_network, ip:"192.168.105.11", virtualbox__intnet: "Red_interna"

  end

end
</pre>

h3. *servidor vpn*

Despues empezaremos levantando el servidor vpn y accediendo a el, despues actualizaremos el sistema e instalaremos openvpn

<pre>
apt update
apt upgrade
apt install openvpn
</pre>

A continuacion crearemos la clave privada del servidor vpn con el comando y parametros siguientes

<pre>
openssl genrsa -out pcert-ser.key 4096
</pre>

Ahora creamos un archivo de configuracion para crear el csr

<pre>
[ req ]
default_bits       = 4096
default_keyfile    = pcert-ser.key #name of the keyfile
distinguished_name = req_distinguished_name

[ req_distinguished_name ]
countryName                 = Country Name (2 letter code)
countryName_default         = ES
stateOrProvinceName         = State or Province Name (full name)
stateOrProvinceName_default = Sevilla
localityName                = Locality Name (eg, city)
localityName_default        = Dos Hermanas
organizationName            = Organization Name (eg, company)
organizationName_default    = IES Gonzalo Nazareno
commonName                  = Common Name (e.g. server FQDN or YOUR name)
commonName_default        = server
commonName_max              = 64
<pre>

Y creamos el csr usando el fichero creado anteriormente

<pre>
openssl req -new -nodes -sha256 -key pcert-ser.key -config genkey.conf -out cert-ser.csr
</pre>

Despues dicho csr lo firmamos (certificado del servidor vpn)

<pre>
openssl x509 -req -days 360 -in /root/ca/csr/cert-ser.csr -CA /root/ca/certs/cert-ac.pem -CAkey /root/ca/private/ac.pkey.pem  -CAcreateserial -out /root/ca/crl/cert-ser.crt
</pre>

Despues crearemos la clave Diffie Hellman, para ello introducimos el comando y parametros siguientes

<pre>
openssl dhparam -out dhserver.pem 2048
</pre>

A continuacion accederemos a "/etc/openvpn/server" y crearemos un fichero llamado servidor.conf con el siguiente contenido

<pre>
#Dispositivo de túnel
dev tun
    
#Direcciones IP virtuales
server 10.99.99.0 255.255.255.0 

#subred local
push "route 192.168.105.0 255.255.255.0"

# Rol de servidor
tls-server

#Parámetros Diffie-Hellman
dh /etc/openvpn/server/dhserver.pem

#Certificado de la CA
ca /etc/openvpn/server/cert-ac.pem

#Certificado local
cert /etc/openvpn/server/cert-ser.crt

#Clave privada local
key /etc/openvpn/server/pcert-ser.key

#Activar la compresión LZO
comp-lzo

#Detectar caídas de la conexión
keepalive 10 60

#Nivel de información
verb 3
</pre>

Por ultimo levantamos el servicio y habilitaremos el forward

<pre>
systemctl stop openvpn-server@servidor
</pre>

Si queremos que se levante al iniciarse el servidor introducimos el siguiente comando

<pre>
systemctl enable openvpn-server@servidor
</pre>

Para activar el forward en el servidor, tenemos que activar el bit de forwarding, para ello nos vamos a "/etc/sysctl.conf" y añadimos la siguiente linea

<pre>
net.ipv4.ip_forward=1
</pre>

Despues para recargar la configuracion de sysctl ejecutamos el comando siguiente

<pre>
sudo sysctl -p
</pre>

Prueba de funcionamiento

!routes-server.png!




h3. *cliente vpn*

A continuacion levantaremos el cliente vpn y accediendo a el, despues actualizaremos el sistema e instalaremos openvpn

<pre>
apt update
apt upgrade
apt install openvpn
</pre>

A continuacion crearemos la clave privada del cliente vpn con el comando y parametros siguientes

<pre>
openssl genrsa -out pcert-cli.key 4096
</pre>

Ahora creamos un archivo de configuracion para crear el csr

<pre>
[ req ]
default_bits       = 4096
default_keyfile    = pcert-cli.key #name of the keyfile
distinguished_name = req_distinguished_name

[ req_distinguished_name ]
countryName                 = Country Name (2 letter code)
countryName_default         = ES
stateOrProvinceName         = State or Province Name (full name)
stateOrProvinceName_default = Sevilla
localityName                = Locality Name (eg, city)
localityName_default        = Dos Hermanas
organizationName            = Organization Name (eg, company)
organizationName_default    = IES Gonzalo Nazareno
commonName                  = Common Name (e.g. server FQDN or YOUR name)
commonName_default        = client
commonName_max              = 64
<pre>

Y creamos el csr usando el fichero creado anteriormente

<pre>
openssl req -new -nodes -sha256 -key pcert-cli.key -config genkey.conf -out cert-cli.csr
</pre>

Despues dicho csr lo firmamos (certificado del servidor vpn)

<pre>
openssl x509 -req -days 360 -in /root/ca/csr/cert-cli.csr -CA /root/ca/certs/cert-ac.pem -CAkey /root/ca/private/ac.pkey.pem  -CAcreateserial -out /root/ca/crl/cert-cli.crt
</pre>










############################################################################

VPN sitio a sitio con OpenVPN y certificados x509

############################################################################
############################################################################