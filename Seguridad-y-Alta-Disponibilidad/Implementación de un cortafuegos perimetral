
_antes de hacer el ejecicio he impedido que se acceda a cualquier servicio o maquina de fuera y dentro de la red, para ello he ejecutado las siguientes lineas en el mismo orden_

<pre>
iptables -F
iptables -t nat -F
iptables -Z
iptables -t nat -Z
iptables -A INPUT -s 172.22.0.0/16 -p tcp -m tcp --dport 22 -j ACCEPT
iptables -A OUTPUT -d 172.22.0.0/16 -p tcp -m tcp --sport 22 -j ACCEPT
iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP
</pre>


Empezaremos permitiendo las conexiones ssh desde los equipos de la red, para ello ejecutamos estas dos lineas

<pre>
iptables -A INPUT -p tcp -i eth1 -s 192.168.100.0/24 --sport 22 -j ACCEPT
iptables -A OUTPUT -p tcp -o eth1 -d 192.168.100.0/24 --dport 22 -j ACCEPT
</pre>

Prueba de funcionamiento

_Antes de aplicar dichas lineas_

<pre>
ssh: connect to host 192.168.100.10 port 22: Connection timed out
</pre>

_Despues de aplicar dichas lineas_

<pre>
Linux lan 4.19.0-6-cloud-amd64 #1 SMP Debian 4.19.67-2+deb10u1 (2019-09-20) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Mon Dec  2 11:09:13 2019 from 192.168.100.2
debian@lan:~$
</pre>



Despues instalaremos un servidor de correos y permitiremos el acceso desde el exterior y desde la maquina que actua como cortafuegos, para ello ejecutamos las siguientes lineas

<pre>
iptables -A INPUT -i eth1 -s 192.168.100.10 -p tcp --sport 25 -j ACCEPT
iptables -A OUTPUT -o eth1 -d 192.168.100.10 -p tcp --dport 25 -j ACCEPT
iptables -t nat -A PREROUTING -p tcp --dport 25 -i eth0 -j DNAT --to 192.168.100.10:25
iptables -A FORWARD -i eth0 -o eth1 -d 192.168.100.10 -p tcp --dport 25 -j ACCEPT
iptables -A FORWARD -i eth1 -o eth0 -s 192.168.100.10 -p tcp --sport 25 -j ACCEPT
</pre>

Prueba de funcionamiento desde el interior

_Antes_

<pre>
Trying 192.168.100.10...
telnet: Unable to connect to remote host: Connection timed out
</pre>


_Despues_

<pre>
Trying 192.168.100.10...
Connected to 192.168.100.10.
Escape character is '^]'.
</pre>


Prueba de funcionamiento desde el exterior

_Antes_

<pre>
Trying 172.22.201.42...
telnet: Unable to connect to remote host: Connection timed out
</pre>


_Despues_

<pre>
Trying 172.22.201.42...
Connected to 172.22.201.42.
Escape character is '^]'.
</pre>


A continuacion configuraremos el cortafuego para que permita que desde el exterior de la red pueda hacer ssh a las maquinas del interior, para ello ejecutamos las siguientes lineas

<pre>
iptables -A FORWARD -i eth0 -o eth1 -d 192.168.100.10/32 -p tcp --dport 22 -j ACCEPT
iptables -A FORWARD -i eth1 -o eth0 -s 192.168.100.10/32 -p tcp --sport 22 -j ACCEPT
iptables -A PREROUTING -t nat -p tcp --dport 22 -j DNAT --to 192.168.100.10:22
</pre>

Prueba de funcionamiento

_Antes_

<pre>
machde@PC-machde:~$ ssh debian@172.22.200.213 -v
OpenSSH_7.9p1 Debian-10+deb10u1, OpenSSL 1.1.1d  10 Sep 2019
debug1: Reading configuration data /etc/ssh/ssh_config
debug1: /etc/ssh/ssh_config line 19: Applying options for *
debug1: Connecting to 172.22.200.213 [172.22.200.213] port 22.
debug1: connect to address 172.22.200.213 port 22: Connection timed out
ssh: connect to host 172.22.200.213 port 22: Connection timed out
</pre>


_Despues_

<pre>
machde@PC-machde:~$ ssh debian@172.22.200.213 -v
OpenSSH_7.9p1 Debian-10+deb10u1, OpenSSL 1.1.1d  10 Sep 2019
debug1: Reading configuration data /etc/ssh/ssh_config
debug1: /etc/ssh/ssh_config line 19: Applying options for *
debug1: Connecting to 172.22.200.213 [172.22.200.213] port 22.
debug1: Connection established.
</pre>

Para ello eliminamos la regla DNAT anterior y añadimos una nueva que redirija por el puerto 2222 los paquetes que viajan hasta el puerto 22, para ello ejecutamos los siguientes comandos
<pre>
iptables -D PREROUTING -t nat -p tcp --dport 22 -j DNAT --to 192.168.100.10:22
iptables -A PREROUTING -t nat -p tcp --dport 2222 -j DNAT --to 192.168.100.10:22
</pre>

Prueba de funcionamiento

_Antes_

<pre>
machde@PC-machde:~$ ssh debian@172.22.200.213 -p 2222 -v
OpenSSH_7.9p1 Debian-10+deb10u1, OpenSSL 1.1.1d  10 Sep 2019
debug1: Reading configuration data /etc/ssh/ssh_config
debug1: /etc/ssh/ssh_config line 19: Applying options for *
debug1: Connecting to 172.22.200.213 [172.22.200.213] port 2222.
debug1: connect to address 172.22.200.213 port 2222: Connection timed out
ssh: connect to host 172.22.200.213 port 2222: Connection timed out
</pre>


_Despues_

<pre>
machde@PC-machde:~$ ssh debian@172.22.200.213 -p 2222 -v
OpenSSH_7.9p1 Debian-10+deb10u1, OpenSSL 1.1.1d  10 Sep 2019
debug1: Reading configuration data /etc/ssh/ssh_config
debug1: /etc/ssh/ssh_config line 19: Applying options for *
debug1: Connecting to 172.22.200.213 [172.22.200.213] port 2222.
debug1: Connection established.
</pre>

#######
    Permite hacer consultas DNS sólo al servidor 192.168.202.2. Comprueba que no puedes hacer un dig @1.1.1.1.
#######

Por ultimo permitiremos hacer consultas DNS solo al servidor 192.168.202.2, para ello ejecutamos los siguientes comandos

<pre>
iptables -A INPUT -i eth1 -o eth0 -p udp --sport 53 -j ACCEPT
iptables -A OUTPUT -i eth0 -o eth1 -p udp --dport 53 -j ACCEPT
iptables -A FORWARD -i eth1 -d 192.168.202.2/32 -p udp --sport 53 -j ACCEPT
iptables -A FORWARD -i eth0 -i 192.168.202.2/32 -p udp --dport 53 -j ACCEPT
</pre>


Prueba de funcionamiento

_Antes_

<pre>
; <<>> DiG 9.11.5-P4-5.1-Debian <<>> @1.1.1.1
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 31674
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 13, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1452
;; QUESTION SECTION:
;.				IN	NS

;; ANSWER SECTION:
.			9008	IN	NS	f.root-servers.net.
.			9008	IN	NS	g.root-servers.net.
.			9008	IN	NS	h.root-servers.net.
.			9008	IN	NS	i.root-servers.net.
.			9008	IN	NS	j.root-servers.net.
.			9008	IN	NS	k.root-servers.net.
.			9008	IN	NS	l.root-servers.net.
.			9008	IN	NS	m.root-servers.net.
.			9008	IN	NS	a.root-servers.net.
.			9008	IN	NS	b.root-servers.net.
.			9008	IN	NS	c.root-servers.net.
.			9008	IN	NS	d.root-servers.net.
.			9008	IN	NS	e.root-servers.net.

;; Query time: 83 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Tue Dec 17 12:08:29 CET 2019
;; MSG SIZE  rcvd: 431
</pre>


_Despues_

<pre>
; <<>> DiG 9.11.5-P4-5.1-Debian <<>> @1.1.1.1
; (1 server found)
;; global options: +cmd
;; connection timed out; no servers could be reached
</pre>

Como se ve no es posible acceder, ahora vamos a probar que puede preguntar al dns (192.168.202.2)

Prueba de funcionamiento

<pre>
root@lan:/home/debian# dig @192.168.202.2

; <<>> DiG 9.11.5-P4-5.1-Debian <<>> @192.168.202.2
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 20870
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 13, AUTHORITY: 0, ADDITIONAL: 27

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
; COOKIE: 18d08beaae688146027000345df8bba437d2c215282e406f (good)
;; QUESTION SECTION:
;.				IN	NS

;; ANSWER SECTION:
.			507695	IN	NS	b.root-servers.net.
.			507695	IN	NS	e.root-servers.net.
.			507695	IN	NS	j.root-servers.net.
.			507695	IN	NS	l.root-servers.net.
.			507695	IN	NS	d.root-servers.net.
.			507695	IN	NS	c.root-servers.net.
.			507695	IN	NS	g.root-servers.net.
.			507695	IN	NS	f.root-servers.net.
.			507695	IN	NS	a.root-servers.net.
.			507695	IN	NS	m.root-servers.net.
.			507695	IN	NS	h.root-servers.net.
.			507695	IN	NS	i.root-servers.net.
.			507695	IN	NS	k.root-servers.net.

;; ADDITIONAL SECTION:
a.root-servers.net.	507695	IN	A	198.41.0.4
b.root-servers.net.	507695	IN	A	199.9.14.201
c.root-servers.net.	507695	IN	A	192.33.4.12
d.root-servers.net.	507695	IN	A	199.7.91.13
e.root-servers.net.	507695	IN	A	192.203.230.10
f.root-servers.net.	507695	IN	A	192.5.5.241
g.root-servers.net.	507695	IN	A	192.112.36.4
h.root-servers.net.	507695	IN	A	198.97.190.53
i.root-servers.net.	507695	IN	A	192.36.148.17
j.root-servers.net.	507695	IN	A	192.58.128.30
k.root-servers.net.	507695	IN	A	193.0.14.129
l.root-servers.net.	507695	IN	A	199.7.83.42
m.root-servers.net.	507695	IN	A	202.12.27.33
a.root-servers.net.	507695	IN	AAAA	2001:503:ba3e::2:30
b.root-servers.net.	507695	IN	AAAA	2001:500:200::b
c.root-servers.net.	507695	IN	AAAA	2001:500:2::c
d.root-servers.net.	507695	IN	AAAA	2001:500:2d::d
e.root-servers.net.	507695	IN	AAAA	2001:500:a8::e
f.root-servers.net.	507695	IN	AAAA	2001:500:2f::f
g.root-servers.net.	507695	IN	AAAA	2001:500:12::d0d
h.root-servers.net.	507695	IN	AAAA	2001:500:1::53
i.root-servers.net.	507695	IN	AAAA	2001:7fe::53
j.root-servers.net.	507695	IN	AAAA	2001:503:c27::2:30
k.root-servers.net.	507695	IN	AAAA	2001:7fd::1
l.root-servers.net.	507695	IN	AAAA	2001:500:9f::42
m.root-servers.net.	507695	IN	AAAA	2001:dc3::35

;; Query time: 1 msec
;; SERVER: 192.168.202.2#53(192.168.202.2)
;; WHEN: Tue Dec 17 11:27:32 UTC 2019
;; MSG SIZE  rcvd: 839
<pre>


*Pregunta a responder*

¿Tendría resolución de nombres y navegación web el cortafuego? ¿Sería necesario?¿Tendrían que estar esas de reglas de forma constante en el cortafuego?

No, ya que la navegacion se realiza mediante ip por lo tanto no es necesario, excepto cuando queremos actualizar los paquetes del sistema de la red ya que funciona mediante nombres, dicho esto no tienen que estar constantemente dichas reglas, solo cuando queramos actualizar el sistema




########################################################################################

iptables -F
iptables -t nat -F
iptables -Z
iptables -t nat -Z
iptables -A INPUT -s 172.22.0.0/16 -p tcp -m tcp --dport 23 -j ACCEPT
iptables -A OUTPUT -d 172.22.0.0/16 -p tcp -m tcp --sport 23 -j ACCEPT
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT

########################################################################################

iptables -F
iptables -t nat -F
iptables -Z
iptables -t nat -Z
iptables -A INPUT -s 172.22.0.0/16 -p tcp -m tcp --dport 23 -j ACCEPT
iptables -A OUTPUT -d 172.22.0.0/16 -p tcp -m tcp --sport 23 -j ACCEPT
iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP
################################################################################
iptables -A FORWARD -i eth0 -o eth1 -d 192.168.100.10/32 -p tcp --dport 22 -j ACCEPT
iptables -A FORWARD -i eth1 -o eth0 -s 192.168.100.10/32 -p tcp --sport 22 -j ACCEPT
iptables -A PREROUTING -t nat -p tcp --dport 22 -j DNAT --to 192.168.100.10:22
iptables -A INPUT -p tcp -i eth1 -s 192.168.100.0/24 --sport 22 -j ACCEPT
iptables -A OUTPUT -p tcp -o eth1 -d 192.168.100.0/24 --dport 22 -j ACCEPT
iptables -A INPUT -s 192.168.100.10/32 -i eth1 -p tcp -m tcp --sport 25 -j ACCEPT
iptables -A OUTPUT -d 192.168.100.10/32 -o eth1 -p tcp -m tcp --dport 25 -j ACCEPT
iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 25 -j DNAT --to 192.168.100.10
iptables -A FORWARD -i eth0 -o eth1 -d 192.168.100.10 -p tcp --dport 25 -j ACCEPT
iptables -A FORWARD -i eth1 -o eth0 -s 192.168.100.10 -p tcp --sport 25 -j ACCEPT
iptables -D PREROUTING -t nat -p tcp --dport 22 -j DNAT --to 192.168.100.10:22
iptables -A PREROUTING -t nat -p tcp --dport 2222 -j DNAT --to 192.168.100.10:22
iptables -A FORWARD -i eth1 -d 192.168.202.2/32 -p udp --dport 53 -j ACCEPT
iptables -A FORWARD -i eth0 -s 192.168.202.2/32 -p udp --sport 53 -j ACCEPT
iptables -A INPUT -s 192.168.202.2/32 -p udp --sport 53 -j ACCEPT
iptables -A OUTPUT -d 192.168.202.2/32 -p udp --dport 53 -j ACCEPT
iptables -t nat -A POSTROUTING -s 192.168.100.0/24 -o eth0 -j SNAT --to 10.0.0.7

########################################################################################
########################################################################################
