Empezaremos permitiendo el puerto de ssh (22) para acceder desde el exterior, para ello utilizaremos el comando

<pre>
iptables -A INPUT -s 172.22.0.0/16 -p tcp --dport 22 -j ACCEPT
iptables -A OUTPUT -d 172.22.0.0/16 -p tcp --sport 22 -j ACCEPT
</pre>

Despues Denegaremos el acceso a nuestra web, a un usuario en concreto (mi maquina)

<pre>
iptables -A INPUT -i eth0 -s 172.22.4.180 -m multiport -p tcp --dports 80,443 -j DROP
iptables -A OUTPUT -o eth0 -d 172.22.4.180 -m multiport -p tcp --sports 80,443 -j DROP
</pre>

A continuacion solo permitiremos que pueda hacer consultas a papion-dns (192.168.202.2)

<pre>
iptables -A INPUT -i eth0 -s 192.168.202.2 -p udp --sport 53 -j ACCEPT
iptables -A OUTPUT -o eth0 -d 192.168.202.2 -p udp --dport 53 -j ACCEPT
</pre>

_Ahora hay que denegar todas las entradas y salidas que tenga el puerto 53, para ello utilizamos los comandos_

<pre>
iptables -A INPUT -i eth0 -p udp --sport 53 -j DROP
iptables -A OUTPUT -o eth0 -p udp --dport 53 -j DROP
</pre>

_De esta manera solo permitiremos hacer consultas a papion-dns_

Prueba de funcionamiento

<pre>
; <<>> DiG 9.11.5-P4-5.1-Debian <<>> @1.1.1.1
; (1 server found)
;; global options: +cmd
;; connection timed out; no servers could be reached
</pre>

Ahora no permitiremos que se pueda acceder al servidor web de www.josedomingo.org para ello ejecutamos las siguientes lineas

<pre>
iptables -A OUTPUT -o eth0 -d 137.74.161.90 -p tcp --dport 443 -j DROP
iptables -A INPUT -i eth0 -s 137.74.161.90 -p tcp --sport 443 -j DROP
iptables -A OUTPUT -o eth0 -d 137.74.161.90 -p tcp --dport 80 -j DROP
iptables -A INPUT -i eth0 -s 137.74.161.90 -p tcp --sport 80 -j DROP
</pre>

_Tampoco se podria acceder a fp.josedomingo.org ya que es un alias (al igual que www.josedomingo.org) de playerone.josedomingo.org que esta asociada a la ip que he denegado (137.74.161.90)_

Prueba de funcionamiento

<pre>
curl: (7) Failed to connect to www.josedomingo.org port 80: Connection timed out
curl: (7) Failed to connect to www.josedomingo.org port 443: Connection timed out
curl: (7) Failed to connect to fp.josedomingo.org port 80: Connection timed out
curl: (7) Failed to connect to fp.josedomingo.org port 443: Connection timed out
</pre>

Despues permitiremos mandar un correo usando nuestro servidor de correo bubuino-smtp.gonzalonazareno.org

<pre>
iptables -A INPUT -i eth0 -s 192.168.203.3 -p tcp --sport 25 -j ACCEPT
iptables -A OUTPUT -o eth0 -d 192.168.203.3 -p tcp --dport 25 -j ACCEPT
</pre>

_Dicha ip es la de babuino-smtp_

Prueba de funcionamiento

_antes_

<pre>
debian@maquina:~$ telnet 192.168.203.3 25
Trying 192.168.203.3...
telnet: Unable to connect to remote host: Connection timed out
</pre>


_Despues_

<pre>
debian@maquina:~$ telnet 192.168.203.3 25
Trying 192.168.203.3...
Connected to 192.168.203.3.
Escape character is '^]'.
220 babuino-smtp.gonzalonazareno.org ESMTP Postfix (Debian/GNU)
hola babuino
502 5.5.2 Error: command not recognized
</pre>

_Al final da un error pero es porque introduje "hola babuino" para demostrar que estaba dentro del mismo_


Despues permitiremos el acceso a nuestro servidor de mariadb a un cliente en concreto, para ello ejecutamos las siguientes lineas

<pre>
iptables -A INPUT -s 172.22.4.180 -p tcp --dport 3306 -j ACCEPT
iptables -A OUTPUT -d 172.22.4.180 -p tcp --sport 3306 -j ACCEPT
</pre>


*Cliente que puede acceder*

<pre>
machde@PC-machde:~$ mysql -u seguridad -h 172.22.201.26 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 40
Server version: 10.3.18-MariaDB-0+deb10u1 Debian 10

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
</pre>


*Resto de clientes*

<pre>
debian@web:~$ mysql -u seguridad -h 172.22.201.26 -p
Enter password:
ERROR 2002 (HY000): Can't connect to MySQL server on '172.22.201.26' (115)
</pre>
