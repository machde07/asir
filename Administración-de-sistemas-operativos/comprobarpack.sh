#!/bin/sh

####  Ejecucion primaria  ####

for name in $(dpkg -l | grep '^ii' | tr -s '\t' ' ' | awk '{print $2}')

do
	####  declaracion de variables  ####

		version=$(apt-cache policy $name | grep Installed | awk '{print $2}')
		vrepo=$(sudo apt-cache policy $name | grep "*'***'*" | awk '{print $2}')

	####################################

	####  Ejecucion secundaria  ####

	if [ $version = $vrepo ]
	then

		echo $name

	fi

	################################
done

##############################
