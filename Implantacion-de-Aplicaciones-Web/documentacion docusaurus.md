# Informacion sobre Docusaurus
Docusaurus esta desarrollado en JavaScript y el sistema de plantillas que utuliza es Markdown (.md)
# Instalacion

Antes de instalar debemos de instalar una serie de paquetes:

*Debes de instalar una version de Node superior a 8.x (version 8 inclusive) y Yarn superior a 1.5.* (version 1.5 inclusive)

`sudo apt install node.js`

`sudo apt install npm`

`sudo apt install yarn`

`sudo apt install git`

Una vez instalados los paquetes que vamos a usar, clonamos el repositorio de nuestro servidor de desarrollo con el comando `git clone git@github.com:manuelchacon07/iaw1920.git`, una vez clonado, nos cambiamos de directorio al que nos ha creado (si no le pones ninguna carpeta de destino a git creará una carpeta llamada igual que el nombre del repositorio), en mi caso es iaw1920, despues, descargamos el script de docusaurus con el comando `npm install --global docusaurus-init`, una vez terminado, ejecutamos docusaurus con el comando `docusaurus-init` e inmediatamente nos creara el arbol de directorio de Docusaurus.


##########
3. Configura el generador para cambiar el nombre de tu página, el tema o estilo de la página,… Indica cualquier otro cambio de configuración que hayas realizado. (1 punto)

# Configuración

La configuracion principal se guarda en `website/siteConfig.js`, aunque tenemos tambien el archivo "Footer" que se ubica en `website/core/Footer.js` que guarda el pie de pagina de nuestro sitio, es decir es un pie para todas las paginas que se generen.
Tenemos un archivo llamado "sidebars" ubicado en `website/sidebars.json`, que contiene la estructura de la seccion de documentacion.

Empezaremos modificando el archivo "siteConfig"

Cambiaremos los siguientes parametros:

    title: '[titulo que quieras ponerle a tu pagina]'
    tagline: '[subtitulo que quieras ponerle a tu pagina]',
    projectName: '[nombre de tu pagina]'

Despues en headerLinks, crearemos tantas lineas (usando como base los que ya estan) como botones queramos tener en nuestra barra de navegacion.

En mi caso como voy a crear tres paginas voy a tener las siguientes lineas:

    {home: 'home', label: 'home'},
    {manual: 'manual', label: 'manual'},
    {repository: 'repository', label: 'repository'},


Tambien podremos cambiar los iconos, cambiando la direccion de las lineas de abajo:

  headerIcon: 'img/favicon.ico',
  footerIcon: 'img/favicon.ico',
  favicon: 'img/favicon.ico',

  /* Colors for website */
  colors: {
    primaryColor: '#ad1544',
    secondaryColor: '#790e33',
  },

  // This copyright info is used in /core/Footer.js and blog RSS/Atom feeds.
  copyright: `Copyright © ${new Date().getFullYear()} dino wiki machde`,

  highlight: {
    // Highlight.js theme to use for syntax highlighting in code blocks.
    theme: 'arta',
  },


4. Genera un sitio web estático con al menos 3 páginas. Deben estar escritas en Markdown y deben tener los siguientes elementos HTML: títulos, listas, párrafos, enlaces e imágenes. El código que estas desarrollando, configuración del generado, páginas en markdown,… debe estar en un repositorio Git (no es necesario que el código generado se guarde en el repositorio, evitalo usando el fichero .gitignore). (3 puntos)




5. Explica el proceso de despliegue utilizado por el servicio de hosting que vas a utilizar. (2 puntos)

6. Piensa algún método (script, scp, rsync, git,…) que te permita automatizar la generación de la página (integración continua) y el despliegue automático de la página en el entorno de producción, después de realizar un cambio de la página en el entorno de desarrollo. Muestra al profesor un ejemplo de como al modificar la página se realiza la puesta en producción de forma automática. (3 puntos)