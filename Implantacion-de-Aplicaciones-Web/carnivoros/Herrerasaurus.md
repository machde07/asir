# Herrerasaurus

![Giganotosaurus](/home/machde/Desktop/docuwiki/Herrerasaurus.gif)

El Herrarasaurus era uno de los mayores carnívoros del Triásico, superando al Coelophysis, pero el Postosuchus era más grande, pero más lento.


## Datos

- Nombre: Herrerasaurus
- Significado: Reptil de Herrera
- descubridor: Victoriano Herrera
- Longitud: entre 3 y 6 metros desde la cabeza a la cola
- Altura: 1,8 metros
- Alimentación: Carnívoro
- Vivió: Entre hace 231 millones de años, en Argentina.

## Clasificación zoológica

- Reino: Animales
- Filo: Reptiiles
- Orden: Dinosaurios
- Clase: Saurisquios
- Suborden: Terópodos
- Familia: Herrerasauurios
- Género: Herrerasaurus
- Especie: Herrerasaurus Ischigualastensis

Referencia: [Wikipedia](https://es.wikipedia.org/wiki/Herrerasaurus_ischigualastensis)
